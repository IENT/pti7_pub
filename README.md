<img align="right" src="https://git.rwth-aachen.de/IENT/ient.nb/raw/master/figures/rwth_ient_logo@2x.png" alt="Logo Institut für Nachrichtentechnik | RWTH Aachen University" width="240px">

# Vorbereitungsunterlagen Praktikum Technische Informatik Versuch 7

## Introduction

This repository contains the preparation material for the lab TI problem 7 held by chair of [Institut für Nachrichtentechnik](http://www.ient.rwth-aachen.de) at RWTH Aachen University.

## Usage

Visit the notebook [index.ipynb](index.ipynb) for a table of contents.

* Run the interactive notebooks directly with binder:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2FIENT%2Fpti7_pub.git/master?urlpath=lab/tree/index.ipynb)
* Run the notebooks on your local machine (for the preparation of the lab session this is not necessary):
  * Install [Anaconda](https://www.anaconda.com/) (using pip is also possible, you have to install all the requirements listed in `environment.yml` and install the two `jupyter labextension ...` commands listed below).
  * Download/`git clone --recurse-submodules` this repository to your local disk.
  * It is highly recommended to run the notebooks in an isolated environment. You can create a new environment called `pti7` from the provided `environment.yml` by running `conda env create -f environment.yml` in the Anaconda prompt. This makes sure that all required packages are installed amd don't interfere with the packages in your base environment.
  * Activate this environment with `conda activate pti7`.
  * Run two final commands in the Anaconda prompt (with activated `pti7` environment):

    ```bash
      jupyter labextension install @jupyter-widgets/jupyterlab-manager
      jupyter labextension install jupyter-matplotlib
    ```

  * Finally run JupyterLab  `jupyter lab`. In your browser, JupyterLab should start. You can then open `index.ipynb` for an overview over all notebooks.
  * You can deactivate the environment with `conda deactivate` (and switch back to it with `conda activate pti7`).

## Contact

If you found a bug, please use the [issue tracker](https://git.rwth-aachen.de/IENT/pti7_pub/issues).

In all other cases, please contact [Christian Rohlfing](http://www.ient.rwth-aachen.de/cms/c_rohlfing/). 